#define DEFAULT_COPY_MOVE_CTORS(x) \
        x(const x&) = default;\
        x& operator=(const x&) = default;\
        x(x&&) = default;\
        x& operator=(x&&) = default;

#define DELETE_COPY_MOVE_CTORS(x) \
        x(const x&) = delete;\
        x& operator=(const x&) = delete;\
        x(x&&) = delete;\
        x& operator=(x&&) = delete;