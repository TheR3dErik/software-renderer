#include <iostream>
#include <SDL2/SDL.h>
#include <memory>

#include "constructors.h"

class Foo
{
public:
    Foo(int x): useless{ std::make_shared<int>(x) } {}

    DEFAULT_COPY_MOVE_CTORS(Foo);
    //DELETE_COPY_MOVE_CTORS(Foo);

    inline int get_value() const { return *useless; }

private:
    std::shared_ptr<int> useless;
};

int main()
{
    Foo ih { 4 };
    //IntHolder ih2 { ih };

    std::cout << ih.get_value() << std::endl;
}
